# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'string_ext/version'

Gem::Specification.new do |spec|
  spec.name          = "string_ext"
  spec.version       = StringExt::VERSION
  spec.authors       = ["Peter O'Sullivan", "Dave Burt"]
  spec.email         = ["peter.osullivan@allori.edu.au", "dave@beddoes.com.au"]
  spec.description   = %q{Allori string ext for make string data standard}
  spec.summary       = %q{Allori string ext}
  spec.homepage      = "https://bitbucket.org/allori/string_ext"
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 2.0"
  spec.add_development_dependency "rake", "~> 12.3"
  spec.add_development_dependency "rspec", "~> 3.8"
end
