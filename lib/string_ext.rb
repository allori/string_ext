class String
  HONORIFICS = %w(Dr Mr Mrs Miss Ms Sir)
  POSTNOMINALS = %w(Cfp Dip Dipfp Advdipfp Fca Afp Am Mba Fs Fp Jp Afpa Aspaa Cta Fipa B.bus Dipfinplan B Fin Ca Cfp® (fp) Dip.
    Partner Financial Planner Adviser)
  GENERIC_EMAIL_PROVIDERS = %w(bigpond.com bigpond.com.au hotmail.com.au gmail.com bigpond.net.au hotmail.com optusnet.com.au iinet.net.au tpg.com.au ozemail.com.au icloud.com yahoo.com yahoo7.com.au yahoo.com.au internode.on.net netspace.net.au westnet.com.au bigpond.com.au outlook.com mac.com exemail.com.au gmail.com.au hotkey.net.au hotmail.co.uk hotmail.com.au mail.com outlook.com.au ozemail.com.au rocketmail.com y7mail.com ymail.com live.com.au)

  def company?
    %w( \(AUSTRALIA\) LIMITED PTY\. PTY LTD\. LTD P/l Services Corporation Finance PLC
        Financial Associates \(IA\) Associcates Planner Planning Wealth Consulting Cooperative University
        Insurance Capital Management Solutions Broker Superannuation Adviser KNM Accountant Institute
        Specialists Enterprises Advice Administration Group Union Corporate Private Equity Commerce
        Office Accountants Investments Advisers Markets Advisory Investment Trustee Trustees
        Partnership Partners Association Inc Incorporated Trust Accounting Licensee Advisors
        Brokers Benefits Securities Directions Equities Investors Platinum Associated Affinity
        International Strategic Planners Business Custodians Stockbroking Consultants Integrity
        Strategy Proprietary Managed Clients Assurance Nominees Custodian Perpetual Managers
        Alliance Retirement Portfolio Online Mortgage Lifestyle Fiducian Professional Australasia
        Industry Specialist Broking Austbrokers Mortgages Integrated Insight Quadrant Strategists
        Underwriting Agencies Commercial Community Statewide Paradigm Resources Network Marketing
        Professionals Infrastructure Wholesale Ausure Independent Ventures Strategies Financials
        Millennium Countrywide Consolidated Momentum Sovereign Precision Chartered Mgmt
        Progressive Protection Coaching Evolution Insurances Development Agents Retireinvest
        Moneytech Innovative Taxation Consultancy Assoc T/A T/AS A\WC\WN LLC ACN FX AFSL
      ).any? {|str| self =~ /\b#{str}\b/i }
  end

  def strip_all_mturks
    scan(/[^[:space:]]+/).join(' ').sub(/^{}$/, '')
  end

  def strip_all
    scan(/[^[:space:]]+/).join(' ')
  end

  def standardize_website
    sub(%r{^https?://}, '').sub(/^www\./, '').sub(/\/$/, '').sub(/^n\/a$/i, '').downcase
  end

  def remove_whitespace
    delete(" \t\r\n")
  end

  def first_and_last_names_mturks
    return unless self =~ /\w/
    names = sub(/\s+[-–]\s.*$/, '').  # delete anything after hyphen or dash
      sub(/\s*\(.*?\)\s*$/, '').    # delete final parentheses
      gsub(/[\(\)]/, '').           # delete other parentheses
      split(/[,[:space:]]+/).       # split on any space
      map(&:capitalize)
    names.shift if HONORIFICS.include?(names.first)
    names.pop while POSTNOMINALS.include?(names.last)
    return if names.length < 2
    names.values_at(0, -1)
  end

  def first_and_last_names
    return unless self =~ /\w/
    names = strip.
      gsub(/[\(\)]/, '').     # delete other parentheses
      split(/[,[:space:]]+/).       # split on any space
      map(&:capitalize)
    names.shift if HONORIFICS.include?(names.first)
    return names if names.length < 2
    names.values_at(0, -1)
  end

  def standardize_name
    first_and_last_names&.join(' ')
  end

  def standardize_number
    return unless self =~ /\d/
    scan(/\d+/).join.to_i
  end

  def standardize_phone_number
    return unless self =~ /\d/
    scan(/\d+/).join.to_i.to_s.sub(/^61/, "")
  end

  def display_phone_number
    if self.length == 9 || self.length == 10
      case self[0]
      when "8", "2", "3", "7"
        "0" + self[0,1] + " " + self[1,4] + " " + self[5,4]
      when "4", "5"
        "0" + self[0,3] + " " + self[3,3] + " " + self[6,3]
      else
        self[0,4] + " " + self[4,3] + " " + self[7,3]
      end
    elsif self.length == 6
      self[0,3] + " " + self[3,3]
    else
      return self
    end
  end

  def classify_phone_number
    #https://en.wikipedia.org/wiki/Telephone_numbers_in_Australia
    # 1300 local rate, 1800 free call, 04 05 mobile, 02 03 07 08 landline
    number = self.sub(/\+61/, '0').standardize_number.to_s
    if number.length < 8
      :invalid
    elsif number.length == 8
      :landline
    elsif number[0] =~ /4|5/
      :mobile
    elsif number[0] =~ /2|3|7|8/
      :landline
    elsif number[0..2] =~ /130|180/
      :reduced_rate
    end
  end

  def classify_email(name = nil)
    identifier, domain = self.split('@')
    first_name, last_name = name.to_s.first_and_last_names
    if identifier =~ /accountants|accounts|admin|advice|advise|advisers|advisersupport|appupdates|apsdg|austfinanceinsurance|business|client|clientservices|commissions|contact|contactus|count|enquiries|financial|financialplanning|finplan|help|info|insurance|invest|iselectlife|life|lifeandsuper|lifeguard|lodgements|mail|office|partner|planner|policy|queries|rbsagencyandschoolnetworks|reception|risk|riskadmin|service|support|wealth/i
      :general
    elsif name && identifier =~ /#{first_name}|#{last_name}/i
      :personal
    else
      :probably_personal
    end
  end

  def email_provider
    identifier, domain = self.split('@')
    if GENERIC_EMAIL_PROVIDERS.include?(domain)
      :generic
    else
      :other
    end
  end

  def standardize_business_name
    all_caps = (self == self.upcase)
    split(/\s+/).reject do |word|
      %r( \(AUSTRALIA\) | LIMITED | PTY\. | PTY | LTD\. | LTD | P/l | PL$ )ix =~ word
    end.map do |word|
      if all_caps && word =~ /[aeiouy]/i
        word.downcase.capitalize
      else
        word
      end
    end.join(' ')
  end

  def standardise_business_trading_as
    self.split(/\s t\/as? \s/ix).first
  end

  def standardize_adviser_name
    spe_char_index = self.index(/[\*\(]/)
    self.slice!(spe_char_index..-1) if spe_char_index.present?
    self.strip.sub(/[^a-zA-Z]+$/, ''). # remove chars which aren't a letter from end of string
      sub(/\s+(account|acc|- orphan|orphan|- orphaned|orphaned)$/i, '')
  end

  alias standardise_name standardize_name
  alias standardise_website standardize_website
  alias standardise_number standardize_number
  alias standardise_phone_number standardize_phone_number
  alias standardise_business_name standardize_business_name
  alias standardise_adviser_name standardize_adviser_name

  def titleize
    split.map(&:capitalize).join(' ')
  end

  def address_attributes
    address = self.strip_all

    states = {
      "Australian Capital Territory" => "ACT",
      "Tasmania" => "TAS",
      "Queensland" => "QLD",
      "Northern Territory" => "NT",
      "Western Australia" => "WA",
      "New South Wales" => "NSW",
      "Victoria" => "VIC",
      "South Australia" => "SA",
    }
    state_pattern = /\b(#{Regexp.union(states.keys + states.values)})\b/
    state_matched = address[state_pattern, 1]
    state = states[state_matched] || state_matched  # abbreviate
    postcode = address[/ \b ( \d{4} ) \b \D* $ /x, 1]

    line1 = address.strip_all
    line1.gsub! postcode,  '' if postcode
    line1.gsub! state_matched, '' if state_matched
    line1.gsub! /[,\s]*$/, ''
    unless line1.empty?
      suburb = line1.split(',').last.strip_all
      line1.gsub! suburb, ''
    end
    line1.gsub! /[,\s]*$/, ''

    {
      street: line1,
      suburb: suburb.to_s.titleize,
      state: state,
      postcode: postcode,
    }
  end

end
